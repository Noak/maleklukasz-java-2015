import java.util.*;



public  class Zad4{
	public static void main(String[] args){
	Student mateusz=new Student("Kworek",1994,"Automatyka");
	mateusz.show();
	mateusz.toStringKierunek("Informatyka");
	mateusz.show();
	
	Nauczyciel olga=new Nauczyciel("Deworek",1962,1000000);
	olga.show();
	
	

	}



static  class Osoba{

	public Osoba(String nazwisko,int rokUrodzenia)
	{
	this.nazwisko=nazwisko;
	this.rokUrodzenia=rokUrodzenia;
	}
	public int rokUrodzenia(){
	return rokUrodzenia;
	}
	public String nazwisko(){
	return nazwisko;
	}
	private String nazwisko;
	private int rokUrodzenia;
}

static class Nauczyciel extends Osoba
{

	public Nauczyciel(String nazwisko,int rokUrodzenia, int pensja)
	{
	super(nazwisko, rokUrodzenia);
	this.pensja=pensja;
	}
		
	public int getPensja()
	{
	return pensja;
	}

	public void show()
   	 {
        System.out.println(nazwisko() + "  " + rokUrodzenia() + "  " + getPensja());
   	 }
	private int pensja;


}

static class Student extends Osoba
{

	public Student(String nazwisko,int rokUrodzenia,String kierunek)
	{
	super(nazwisko, rokUrodzenia);
	this.kierunek=kierunek;
	}
	

	
	public String toStringKierunek(String d)
	{
	kierunek=d;
	return kierunek;
	}
	
	public void show()
   	{
        System.out.println(nazwisko() + "  " + rokUrodzenia() + "  " + kierunek);
   	}
	
	private String kierunek;
}
}



