package pl.imiajd.malek.test;
import java.time.LocalDate;

public class Fortepian extends Instrument
{
    
    
    public Fortepian(String producent, LocalDate rokProdukcji)
    {
        super(producent, rokProdukcji);
    }

    public String getDzwiek()
    {
        return this.dzwiek;
    }

private String dzwiek = "dwiek fortepianu";
}
