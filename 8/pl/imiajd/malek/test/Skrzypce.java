package pl.imiajd.malek.test;
import java.time.LocalDate;

public class Skrzypce extends Instrument
{
    private String dzwiek = "dzwiek skrzypiec";

    public Skrzypce(String producent, LocalDate rokProdukcji)
    {
        super(producent, rokProdukcji);
    }

    public String getDzwiek()
    {
        return this.dzwiek;
    }
}
