package pl.imiajd.malek.test;
import java.time.LocalDate;

public abstract class Instrument
{
    
    public Instrument(String producent, LocalDate rokProdukcji)
    {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public abstract String getDzwiek();

    public boolean equals(Object otherObject)
    {
        if (this == otherObject)
	{
            return true;
        }

        if (otherObject == null)
	{
            return false;
        }

        if (getClass() != otherObject.getClass())
	{
            return false;
        }

        Instrument other = (Instrument) otherObject;

        return producent.equals(other.producent) && rokProdukcji.equals(other.rokProdukcji);
    }

    public String toString()
    {
        return "Producent: " + this.producent + " rok produkcji: " +  this.rokProdukcji.getYear();
    }
    private String producent;
    private LocalDate rokProdukcji;


}
