import java.util.*;

public class Zad3
{
	public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a,ArrayList<Integer> b){
		
	ArrayList<Integer> lista = new ArrayList<Integer>();
		
	lista.addAll(a);
	lista.addAll(b);

	for (int i=1; i<lista.size(); i++){
	int tp =lista.get(i);
	int l;
	for (l=i-1; l>=0 && tp<lista.get(l); l--){
	lista.set(l+1,lista.get(i));
		}
	lista.set(l+1,tp);
	}

	return lista;
		}
		
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(3);
        a.add(4);
        a.add(8);
        a.add(9);
        
        ArrayList<Integer> b = new ArrayList<Integer>();
        b.add(5);
        b.add(6);
        
        ArrayList<Integer> c=mergeSorted(a,b);
        System.out.println("A= " + a);
	System.out.println("B= " + b);
        System.out.println("C= " + c);
       

    }
}

