import java.util.*;


public class Zad2
{
	public static void main(String args[])
	{
		IntegerSet zbior1=new IntegerSet();
		IntegerSet zbior2=new IntegerSet();
		IntegerSet zbior3=new IntegerSet();
		IntegerSet zbior4=new IntegerSet();
		zbior1.insertElement(1);
		zbior1.insertElement(2);
		zbior1.insertElement(3);
		zbior2.insertElement(3);
		zbior2.insertElement(5);
		zbior2.insertElement(10);
		zbior2.insertElement(4);
		zbior2.insertElement(8);
		System.out.println("pierwszy zbior : "+zbior1.toString());
		System.out.println("drugi zbior : "+zbior2.toString());
		zbior2.deleteElement(8);
		System.out.println("pierwszy zbior: "+zbior1.toString());
		System.out.println("drugi zbior  "+zbior2.toString());
		zbior3=IntegerSet.union(zbior1,zbior2);
		System.out.println("suma: "+zbior3.toString());
		zbior4=IntegerSet.intersection(zbior1,zbior2);
		System.out.println("iloczyn: "+zbior4.toString());
		System.out.println("1 i 2 takie same ? : " +zbior1.equals(zbior2));
	}
}

class IntegerSet
{
	public IntegerSet()
	{
		 set=new boolean[100];
		
		for (int i=0;i<set.length;i++)
		set[i]=false;
	}
	
	public static IntegerSet union(IntegerSet a,IntegerSet b)
	{
		IntegerSet wynik = new IntegerSet();
        for (int i=0; i<100; i++)
		{
			wynik.set[i] = (a.set[i] || b.set[i]);
		}

		return wynik;
	}
	
	public static IntegerSet intersection(IntegerSet a, IntegerSet b)
	{
	 IntegerSet wy = new IntegerSet();
        for (int i=0; i<100; i++)
		{
			wy.set[i] = (a.set[i] && b.set[i]);
		}

		return wy;
	}

	public void insertElement(int x){
		if(x>=0 && x<=100)
		this.set[x]=true;
	}
	
	
	public void deleteElement( int x)
	{
	this.set[x]=false;
	} 
	
	public String toString()
	{
		String str=" ";
		for(int i=0;i<100;i++)
		{
		if(this.set[i]==true)
		str=str.concat(i +" ");
		}
		return str;
	}
	public boolean equals(IntegerSet a)
	{
		for (int i=0; i<100; i++){
			if(set[i] != a.set[i])
			return false;
			
		}
		return true;
	}
	boolean[] set;
	
}
