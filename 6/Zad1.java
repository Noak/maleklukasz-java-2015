import java.util.*;

public class Zad1{
	public static void main(String args[])
	{
	RachunekBankowy saver1=new RachunekBankowy(2000);
	RachunekBankowy saver2=new RachunekBankowy(3000);
		
        saver1.setRocznaStopaProcentowa(0.04);
        
        System.out.println(saver1.obliczMiesieczneOdsetki());
	    System.out.println(saver2.obliczMiesieczneOdsetki());

		saver1.setRocznaStopaProcentowa(0.05);
		
		
        System.out.println(saver1.obliczMiesieczneOdsetki());
	    System.out.println(saver2.obliczMiesieczneOdsetki());
		
	}
}

class RachunekBankowy
{
	public RachunekBankowy(double saldo)
	{
		
		this.saldo=saldo;
	}
	
		public double obliczMiesieczneOdsetki()
		{
			saldo=saldo+(saldo*rocznaStopaProcentowa)/12;
			return saldo;
		}
		
		public static void setRocznaStopaProcentowa(double stopa)
		{
			rocznaStopaProcentowa=stopa;
		}
		public static double rs()
		{
			return rocznaStopaProcentowa;
		}
		public static double rocznaStopaProcentowa;
		private double saldo;
}
