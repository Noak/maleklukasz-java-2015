import java.util.*;
import pl.imiajd.malek.Osoba;
import java.time.LocalDate;


public class Zad2
{
    public static void main(String[] args){


        ArrayList<Osoba> grupa = new ArrayList<>();
	 LocalDate b5= LocalDate.parse("2015-06-05");
	LocalDate b = LocalDate.parse("1934-01-11");
        LocalDate b2 = LocalDate.parse("1977-12-12");
        LocalDate b3 = LocalDate.parse("1998-02-23");
        LocalDate b4 = LocalDate.parse("1994-11-28");
        Osoba osoba1 = new Osoba("Marecki",b);
        Osoba osoba2 = new Osoba("Marecki",b2);
        Osoba osoba3 = new Osoba("Kubica",b3);
        Osoba osoba4 = new Osoba("Kukiz",b4);
        Osoba osoba5 = new Osoba("Malek",b5);
        grupa.add(osoba1);
        grupa.add(osoba2);
        grupa.add(osoba3);
        grupa.add(osoba4);
        grupa.add(osoba5);
        for (Osoba j : grupa)
	{
            System.out.println(j.toString());

            System.out.println("Lat: " + j.ileLat() + ", " + "Miesięcy: " + j.ileMiesiecy() + ", " + "Dni: " + j.ileDni() );
        }
        System.out.println();

        Osoba[] d = new Osoba[grupa.size()];
        d = grupa.toArray(d);
        Arrays.sort(d);

        for (Osoba t : d )
	{
            System.out.println(t.toString());
        }

        System.out.println();

        
    }
}

