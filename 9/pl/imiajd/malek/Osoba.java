package  pl.imiajd.malek;
import java.util.*;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.LocalDate;

    
public  class Osoba implements Comparable<Osoba>{
    
    public Osoba(String nazwisko,LocalDate dataUrodzenia)
    {
     this.nazwisko=nazwisko;
     this.dataUrodzenia=dataUrodzenia;   
        
    }
    
    
    
    
public String toString()
    {
    return getClass().getName() + "[" +this.nazwisko+" "+this.dataUrodzenia+ "]";    
    
    }
    
public boolean equals(Object otherObject)
    {
	
    if (this == otherObject) return true;
	if (otherObject == null) return false;
	if (getClass() != otherObject.getClass()) return false;

   Osoba other = (Osoba) otherObject;

        // sprawdzamy czy pola mają identyczne wartości
        return nazwisko.equals(other.nazwisko)
               && dataUrodzenia.equals(other.dataUrodzenia);

	 }
	 public int compareTo(Osoba other)
	 {
		if(nazwisko.compareTo(other.nazwisko)>0)
		{
		return 1;
		}
		else if(nazwisko.compareTo(other.nazwisko)<0)
		{	
		return -1;
		}
		else if(dataUrodzenia.compareTo(other.dataUrodzenia)>0)
		{
		return 1;
		}
		else if(dataUrodzenia.compareTo(other.dataUrodzenia)<0)
		{	
		return -1;
		}
		else
		{
		return 0;
		}
	}

   public int ileLat()
    {
        int wynik = Period.between(dataUrodzenia, LocalDate.now()).getYears();
        return wynik;
    }

    public int ileMiesiecy()
    {
        int wynik = Period.between(dataUrodzenia, LocalDate.now()).getMonths();
        return wynik;
    }

    public int ileDni()
    {
        int wynik = Period.between(dataUrodzenia,LocalDate.now()).getDays();
        return wynik;
    }
    

private String nazwisko;
private LocalDate dataUrodzenia;

}
