import java.util.*;

public class Zad1f{
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println("Podaj ilosc elemenetow: ");
		int n = s.nextInt();
		int[] tab = new int[n];
		
		Random r = new Random();
		for(int i=0;i<tab.length;i++)
		{
			tab[i] = r.nextInt(1999)-999;
		}
		
		System.out.print("wygenerowana tablica: ");
		for(long el : tab)
		{
			System.out.print(el + " ");
		}
		
		for(int i=0;i<tab.length;i++)
		{
			if(tab[i]>0)
			{
				tab[i] = 1;
			}
			if(tab[i]<0)
			{
				tab[i] = -1;
			}
		}
		System.out.println();
		System.out.print("zmieniona tablica: ");
		for(long el : tab){
		System.out.print(el + " ");
		}
		System.out.println();
	}
}
