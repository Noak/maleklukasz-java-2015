import java.util.*;

public class Zad2c{
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println(" ilosc elementow: ");
		int n = s.nextInt();
		if(n<1 || n>100)
		{
		System.out.println("bledny przedzial");
		System.exit(1);
		}
		int[] tab = new int[n];
		
		generuj(tab,999,-999);
		System.out.print("Losowo wygenerowana tablica: ");
		wypisz(tab);
		System.out.println("Ilosc maksymalnych: " + ileMaksymalnych(tab));
	}
	public static void generuj(int[] tab, int max, int min){
		Random r = new Random();
		for(int i=0;i<tab.length;i++){
			tab[i]=r.nextInt((max-min)+1)+min;
		}
	}
	public static void wypisz(int[] tab){
		for(long el : tab){
			System.out.print(el + " ");
		}
		System.out.println();
	}
	public static int ileMaksymalnych(int[] tab){
		int licz = 0;
		int maks = tab[0];
		for(int i=0;i<tab.length;i++)
		{
			if(tab[i]>maks)
			{
			maks = tab[i];
			}
		}
		for(int i = 0;i<tab.length;i++)
		{
			if(tab[i]==maks)
			{
			licz++;
			}
		}
		return licz;
	}
		
}
