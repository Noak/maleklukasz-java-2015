import java.util.*;

public class Zad2a{
	public static void wypisz(int[] tab){
		for(long el : tab){
		System.out.print(el + " ");
		}
	}
	
	public static void generuj(int[] tab, int n){
		Random r = new Random();
		for(int i=0;i<tab.length;i++)
		{
		tab[i] = r.nextInt(1999)-999;
		}
	}
	public static int ileNieparzystych(int[] tab){
		int li = 0;
		for(int i=0;i<tab.length;i++)
		{
			if(tab[i]%2!=0)
			{
			li++;
			}
		}
		return li;
	}
	
	public static int ileParzystych(int[] tab){
		int li = 0;
		for(int i=0;i<tab.length;i++){
			if(tab[i]%2==0){
			li++;
			}	
		}
		return li;
	}
	public static void main (String[] args) {
		Scanner s = new Scanner(System.in);	
		System.out.print("Podaj n: ");
		int n = s.nextInt();
		if(n<1 || n>100)
		{
		System.out.print("bledne dane");
		System.out.println();
		System.exit(1);
		}
			
	int[] tab = new int[n];
	generuj(tab, n);
	wypisz(tab);
		
	System.out.println();
	System.out.print("nieparzystych: ");
	ileNieparzystych(tab);
	System.out.println();
	System.out.print("parzystych: " + ileParzystych(tab));
		
	}
}
		
