import java.util.*;

public class Zad2g{
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println("Podaj ilosc elemetow: ");
		int n = s.nextInt();
		if(n<1 || n>100)
		{
		System.out.println("zly przedzial");
		System.exit(1);
		}
		int[] tab = new int[n];
		generuj(tab, 999, -999);
		System.out.print("Losowa tablica: ");
		wypisz(tab);
		System.out.println("Podaj lewy element: ");
		int lewy = s.nextInt();
		System.out.println("Podaj prawy element: ");
		int prawy = s.nextInt();
		odwrocFragment(tab, lewy, prawy);
		System.out.print("tablica po odwroceniu: ");
		wypisz(tab);
		
	}
	public static void generuj(int[] tab, int max, int min)
	{
		Random r=new Random();
		for(int i=0;i<tab.length;i++)
		{
		tab[i]= r.nextInt((max-min)+1)+min;
		}
	}
	public static void wypisz(int[] tab){
		for(long el: tab) System.out.print(el + " ");
		System.out.println();
	}
	public static void odwrocFragment(int[] tab, int lewy, int prawy){
		int x = 0;
		while(lewy<prawy)
		{
		tab[lewy] = x;
		tab[lewy] = tab[prawy];
		tab[prawy] = x;
		lewy++;
		prawy--;
		}
	}
}
