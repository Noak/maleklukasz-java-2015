import java.util.*;

public class Zad2b{
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println("ilosc elementow: ");
		int n = s.nextInt();
		if(n<1 || n>100)
		{
		System.out.println("bledny przedzial !");
		System.exit(1);
		}
		int[] tab = new int[n];
		generuj(tab);
		System.out.println("Losowa tablica: ");
		wypisz(tab);
		System.out.println("Dodatnie: " + dodatnie(tab));
		System.out.println("Ujemne: " + ujemne(tab));
		System.out.println("Zerowe: " + zerowe(tab));
	}
	public static void generuj(int[] tab)
		{
		Random r = new Random();
		for(int i=0;i<tab.length;i++){
			tab[i] = r.nextInt(1999)-999;
		}
	}
	public static void wypisz(int[] tab)

		{
		for(long el : tab){
			System.out.print(el + " ");
		}
		System.out.println();
	}
	public static int dodatnie(int[] tab)
		{
		int licznik = 0;
		for(int i = 0;i<tab.length;i++){
			if(tab[i]>0) licznik++;
		}
		return licznik;
	}
	public static int ujemne(int[] tab)
		{
		int licznik = 0;
		for(int i=0;i<tab.length;i++){
			if(tab[i]<0) licznik++;
		}
		return licznik;
	}
	public static int zerowe(int[] tab)
		{
		int licznik = 0;
		for(int i=0;i<tab.length;i++){
			if(tab[i]==0) licznik++;
		}
		return licznik;
	}
}
