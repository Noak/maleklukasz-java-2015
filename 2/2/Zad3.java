import java.util.Scanner;

public class Zad3{
	public static void main(String[] args){
		Scanner in = new Scanner(System.in); 
		System.out.print("Podaj ilosc: ");
		int n = in.nextInt();
		double a;
		int dodatnie=0; 
		int ujemne=0;
		int zero=0;
		for(int i=0;i<n;i++){
			System.out.print("Podaj kolejna liczbe : ");		
			a = in.nextInt();
			if (a>0) dodatnie+=1;
			if (a<0) ujemne+=1;
			if (a==0) zero+=1;
		}
		System.out.println("Dodatnie : "+dodatnie);
		System.out.println("Ujemne : "+ujemne);
		System.out.println("Zera : "+zero);
		
	}
}
