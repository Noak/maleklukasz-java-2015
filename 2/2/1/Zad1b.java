import java.util.Scanner;
import java.util.Arrays;
public class Zad1b {
  public static void main(String[] args){
		System.out.print("Podaj ilosc liczb: ");
		Scanner in = new Scanner(System.in);
    		int n = in.nextInt();
	
		long[] tab = new long[n];		
		
		wypelnij(tab,n);
		podzielne(tab);
}
public static void wypelnij(long[] tab,int n){
	Scanner in = new Scanner(System.in);
		int el;
    	for(int i=0;i<n;i++){
			System.out.print("Podaj  element: ");
			el=in.nextInt();
			tab[i]=el;
		}	


	}
public static void podzielne(long[] tab){
		int suma=0;
    	for(int i=0;i<tab.length;i++){
			if ((tab[i]%3==0) && (tab[i]%5!=0))  suma+=1;
		}	
		System.out.println("Podzielne przez 3 i niepodzielne przez 5 "+suma);
	}

}
