import java.util.*;
import java.lang.*;

public class Zad1f{
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println("Podaj napis: ");
		String str = s.nextLine();
		System.out.println("Dzialanie change: " + change(str));
	}
	public static String change(String str){
		StringBuffer napis = new StringBuffer("");
		char c;
		for (int i=0; i<str.length();++i){
		c = str.charAt(i);
		if (Character.isLowerCase(c)) napis.append(Character.toUpperCase(c));
		if(Character.isUpperCase(c)) napis.append(Character.toLowerCase(c));
		}
				
		return napis.toString();
	}
}
