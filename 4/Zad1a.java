import java.util.Scanner;


public class Zad1a{
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println("Podaj napis: ");
		String str = s.nextLine();
		System.out.println("Podaj znak: ");
		char cd = 'a';
		
		System.out.println("Wystapien znaku " + cd + " w napisie " + str + " jest rowne " + countChar(str, cd));
		
	}
	public static int countChar(String str, char cd){
		char znak;
		int ilosc = 0;
		for(int i=0;i<str.length();i++){
			znak = str.charAt(i);
			if(znak == cd) ilosc ++;
		}
		return ilosc;	
	}
}
	


