import java.util.*;

public class Zad1b{
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println("Podaj  str, a pozniej subStr: ");
		String str = s.nextLine();
		String subStr = s.nextLine();
		System.out.println(" Ilosc wystapien napisu subStr w napisie str wynosi " + countSubStr(str, subStr));
	}
	public static int countSubStr(String str, String subStr){
		int ilosc = 0;
		for (int loc = str.indexOf(subStr); loc != -1;
		     loc = str.indexOf(subStr, loc + subStr.length()))
		     ilosc++;
		return ilosc;
	}
}
	
