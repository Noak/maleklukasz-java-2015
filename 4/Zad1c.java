import java.util.*;

public class Zad1c{
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println("Podaj napis: ");
		String str = s.nextLine();
		System.out.println("Dzialanie funkcji: " + middle(str));
		
	}
	public static String middle(String str){
		int dlugosc = str.length();
		String wy = str;
		if(dlugosc%2!=0){
			wy = str.substring((dlugosc/2),(dlugosc/2)+1);
		}
		if(dlugosc%2==0){
			wy = str.substring((dlugosc/2-1),(dlugosc/2)+1);
		}
		return wy;
	}
}
